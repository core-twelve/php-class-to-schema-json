<?php


namespace Core12\JsonSchema;


class JsonSchema implements \JsonSerializable
{
    const TYPE_OBJECT = 'object';

    /**
     * @var string
     */
    private $schema;
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $type;
    /**
     * @var Property[]
     */
    private $properties = [];

    /**
     * JsonSchema constructor.
     * @param string $schema
     * @param string $id
     * @param string $type
     */
    public function __construct($schema, $id, $type)
    {
        $this->schema = $schema;
        $this->id = $id;
        $this->type = $type;
    }

    /**
     * @param Property $property
     */
    public function addProperty($property)
    {
        $this->properties[$property->getName()] = $property;
    }

    public function jsonSerialize()
    {
        return [
            '$schema'               => $this->schema,
            'id'                    => $this->id,
            'type'                  => $this->type,
            'additionalProperties'  => false,
            'properties'            => $this->properties
        ];
    }

    public function __toString()
    {
        return json_encode($this);
    }
}