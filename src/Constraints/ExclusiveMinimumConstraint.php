<?php


namespace Core12\JsonSchema\Constraints;

use Core12\JsonSchema\Constraint;

/**
 * Class ExclusiveMinimumConstraint
 * Number constraint
 * @package Core12\JsonSchema\Constraints
 */
class ExclusiveMinimumConstraint extends Constraint
{
    /**
     * @var boolean
     */
    private $value;

    /**
     * ExclusiveMaximumConstraint constructor.
     * @param bool $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    public function jsonSerialize()
    {
        return true;
    }

    public function getName()
    {
        return 'exclusiveMinimum';
    }
}