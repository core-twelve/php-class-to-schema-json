<?php


namespace Core12\JsonSchema\Constraints;

use Core12\JsonSchema\Constraint;

/**
 * Class MaxLengthConstraint
 * String constraint
 * @package Core12\JsonSchema\Constraints
 */
class MaxLengthConstraint extends Constraint
{
    /**
     * @var int
     */
    private $maxLength;

    /**
     * MaxLengthConstraint constructor.
     * @param int $maxLength
     */
    public function __construct($maxLength)
    {
        $this->maxLength = $maxLength;
    }

    public function jsonSerialize()
    {
        return $this->maxLength;
    }

    public function getName()
    {
        return 'maxLength';
    }
}