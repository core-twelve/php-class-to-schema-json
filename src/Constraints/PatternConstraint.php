<?php


namespace Core12\JsonSchema\Constraints;

use Core12\JsonSchema\Constraint;

final class PatternConstraint extends Constraint
{
    /**
     * @var string
     */
    private $pattern;

    /**
     * PatternConstraint constructor.
     * @param string $pattern
     */
    public function __construct($pattern)
    {
        $this->pattern = $pattern;
    }

    public function jsonSerialize()
    {
        return $this->pattern;
    }

    public function getName()
    {
        return 'pattern';
    }
}