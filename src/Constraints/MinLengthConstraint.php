<?php


namespace Core12\JsonSchema\Constraints;

use Core12\JsonSchema\Constraint;

/**
 * Class MinLengthConstraint
 * String constraint
 * @package Core12\JsonSchema\Constraints
 */
class MinLengthConstraint extends Constraint
{
    /**
     * @var int
     */
    private $minLength;

    /**
     * MinLengthConstraint constructor.
     * @param int $minLength
     */
    public function __construct($minLength)
    {
        $this->minLength = $minLength;
    }

    public function jsonSerialize()
    {
        return $this->minLength;
    }

    public function getName()
    {
        return 'minLength';
    }
}