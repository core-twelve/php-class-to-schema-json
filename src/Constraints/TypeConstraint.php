<?php


namespace Core12\JsonSchema\Constraints;

use Core12\JsonSchema\Constraint;

final class TypeConstraint extends Constraint
{
    /**
     * @var string
     */
    private $type;

    /**
     * TypeConstraint constructor.
     * @param string $type
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    public function jsonSerialize()
    {
        return $this->type;
    }

    public function getName()
    {
        return 'type';
    }
}