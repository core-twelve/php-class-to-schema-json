<?php


namespace Core12\JsonSchema\Constraints;

use Core12\JsonSchema\Constraint;

/**
 * Class MaximumConstraint
 * Number constraint
 * @package Core12\JsonSchema\Constraints
 */
class MaximumConstraint extends Constraint
{
    /**
     * @var integer
     */
    private $value;

    /**
     * MaximumConstraint constructor.
     * @param int $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    public function jsonSerialize()
    {
        return $this->value;
    }

    public function getName()
    {
        return 'maximum';
    }
}