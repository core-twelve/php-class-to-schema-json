<?php


namespace Core12\JsonSchema\Constraints;

use Core12\JsonSchema\Constraint;

/**
 * Class ExclusiveMaximumConstraint
 * Number constraint
 * @package Core12\JsonSchema\Constraints
 */
class ExclusiveMaximumConstraint extends Constraint
{
    /**
     * @var boolean
     */
    private $value;

    /**
     * ExclusiveMaximumConstraint constructor.
     * @param bool $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    public function jsonSerialize()
    {
        return $this->value;
    }

    public function getName()
    {
        return 'exclusiveMaximum';
    }
}