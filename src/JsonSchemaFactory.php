<?php
namespace Core12\JsonSchema;

use Core12\JsonSchema\ConstraintFactory;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Mapping\MetadataInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class JsonSchemaFactory
 * @package Core12\JsonSchema
 */
class JsonSchemaFactory
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * JsonSchemaFactory constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param string $class
     *
     * @return JsonSchema
     */
    public function generateFromClass($class)
    {
        $schema = new JsonSchema(
            "http://json-schema.org/draft-03/schema#",
            "urn:product#",
            JsonSchema::TYPE_OBJECT
        );

        /** @var ClassMetadata $metadata */
        $metadata = $this->validator->getMetadataFor($class);

        $constraintFactory = new ConstraintFactory();

        foreach ($metadata->getConstrainedProperties() as $propertyName) {
            $propertyMetadata = $metadata->getPropertyMetadata($propertyName)[0];
            $asserts = $propertyMetadata->getConstraints();

            $schemaProperty = new Property($propertyName);
            foreach ($asserts as $assert) {
                $constraints = $constraintFactory->generateFromSymfonyAssert($assert);
                foreach ($constraints as $constraint) {
                    $schemaProperty->addConstraint($constraint);
                }
            }
            $schema->addProperty($schemaProperty);
        }

        return $schema;
    }
}