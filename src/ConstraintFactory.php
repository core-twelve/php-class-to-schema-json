<?php


namespace Core12\JsonSchema;


use Core12\JsonSchema\Exception\ConstraintNotAvailableException;
use Core12\JsonSchema\Constraints;
use Core12\JsonSchema\Exception\ConstraintNotFullyImplementedException;
use Symfony\Component\Validator\Constraint as SymfonyAssert;
use Symfony\Component\Validator\Constraints as Assert;

class ConstraintFactory
{
    /**
     * @var callable[]
     */
    private $fabricMethods;

    public function __construct()
    {
        $this->fabricMethods = [

            Assert\Type::class => function(Assert\Type $assert) {
                $map = [
//        'array'     => ,
//        'bool',
//        'callable',
//        'float',
//        'double',
                    'int'       => 'integer',
                    'integer'   => 'integer',
//        'long',
//        'null',
//        'numeric',
//        'object',
//        'real',
//        'resource',
//        'scalar',
                    'string'    => 'string'

                    //alnum
                    //alpha
                    //cntrl
                    //digit
                    //graph
                    //lower
                    //print
                    //punct
                    //space
                    //upper
                    //xdigit
                ];

                if (!isset($map[$assert->type])) {
                    throw new ConstraintNotFullyImplementedException("Type {$assert->type} is not implemented yet");
                }

                return [ new Constraints\TypeConstraint($map[$assert->type]) ];
            },


            Assert\Regex::class => function(Assert\Regex $assert) {
                if ($assert->htmlPattern || $assert->match === false) {
                    throw new ConstraintNotAvailableException("Options `htmlPattern` and `match` are not implemented");
                }

                return [ new Constraints\PatternConstraint($assert->pattern) ];
            },


            Assert\Length::class => function(Assert\Length $assert) {
                $res = [];
                if ($assert->min) {
                    $res []= new Constraints\MinLengthConstraint($assert->min);
                }
                if ($assert->max) {
                    $res []= new Constraints\MaxLengthConstraint($assert->max);
                }

                return $res;
            },


            Assert\LessThan::class => function(Assert\LessThan $assert) {
                return [
                    new Constraints\MaximumConstraint($assert->value),
                    new Constraints\ExclusiveMaximumConstraint(true),
                ];
            },


            Assert\LessThanOrEqual::class => function(Assert\LessThanOrEqual $assert) {
                return [
                    new Constraints\MaximumConstraint($assert->value),
                    new Constraints\ExclusiveMaximumConstraint(false),
                ];
            },


            Assert\GreaterThan::class => function(Assert\GreaterThan $assert) {
                return [
                    new Constraints\MinimumConstraint($assert->value),
                    new Constraints\ExclusiveMinimumConstraint(true),
                ];
            },


            Assert\GreaterThanOrEqual::class => function(Assert\GreaterThanOrEqual $assert) {
                return [
                    new Constraints\MinimumConstraint($assert->value),
                    new Constraints\ExclusiveMinimumConstraint(false),
                ];
            },
        ];
    }

    /**
     * @param SymfonyAssert $assert
     *
     * @return Constraint[]
     */
    public function generateFromSymfonyAssert(SymfonyAssert $assert)
    {
        $assertClass = get_class($assert);

        if (isset($this->fabricMethods[$assertClass])) {
            $fabricMethod = $this->fabricMethods[$assertClass];
            $constraints = $fabricMethod($assert);

            if (!is_array($constraints)) {
                throw new \Exception('ConstraintFactory fabric method should return array of Contraint objects');
            }

            return $constraints;
        }

        throw new ConstraintNotAvailableException;
    }

}