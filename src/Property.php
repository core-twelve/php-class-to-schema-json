<?php


namespace Core12\JsonSchema;


class Property implements \JsonSerializable
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $type;
//    private $extends;

    /**
     * @var Constraint[]
     */
    private $constraints;

    /**
     * Property constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @param Constraint $constraint
     */
    public function addConstraint(Constraint $constraint)
    {
        $this->constraints[] = $constraint;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function jsonSerialize()
    {
        $data = [];

        foreach ($this->constraints as $constraint) {
            $data[$constraint->getName()] = $constraint;
        }

        return $data;
    }
}