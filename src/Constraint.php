<?php
namespace Core12\JsonSchema;

abstract class Constraint implements \JsonSerializable
{
    /**
     * @return string
     */
    abstract public function getName();
}