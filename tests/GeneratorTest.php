<?php
namespace Core12\JsonSchema\Tests;


use Composer\Autoload\ClassLoader;
use Core12\JsonSchema\JsonSchemaFactory;
use Core12\JsonSchema\Tests\Sample\Product;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class GeneratorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var JsonSchemaFactory
     */
    private $generator;

    public function setUp()
    {
        /** @var $loader ClassLoader */
        $loader = require __DIR__.'/../vendor/autoload.php';

        $dir = __DIR__."/../vendor/symfony/validator/Constraints";
        AnnotationRegistry::registerAutoloadNamespace("Symfony\Component\Validator\Constraints", $dir);
        AnnotationRegistry::registerLoader([ $loader, 'loadClass' ]);
        $reader = new AnnotationReader();

        $vb = \Symfony\Component\Validator\Validation::createValidatorBuilder();
        $vb->enableAnnotationMapping($reader);

        $this->generator = new JsonSchemaFactory($vb->getValidator());
    }

    public function testGenerator()
    {
        $jsonSchema = $this->generator->generateFromClass(Product::class);

        echo json_encode(json_decode($jsonSchema), JSON_PRETTY_PRINT);

//        $refResolver = new \JsonSchema\RefResolver(new \JsonSchema\Uri\UriRetriever(), new \JsonSchema\Uri\UriResolver());
//        $schema = $refResolver->resolve('http://json-schema.org/draft-03/schema#');
//
//        $validator = new \JsonSchema\Validator();
//        $res = $validator->check($jsonSchema, $schema);
//
//        print_r($res);
    }
}