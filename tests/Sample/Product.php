<?php

namespace Core12\JsonSchema\Tests\Sample;

use Symfony\Component\Validator\Constraints as Assert;

class Product
{
    /**
     * @Assert\Type("string")
     * @Assert\Regex("^\S.*\S$")
     * @Assert\Length(min=3, max=50)
     */
    private $name;

    /**
     * @Assert\Type("integer")
     * @Assert\GreaterThan(0)
     */
    private $price;

    /**
     * @Assert\Type("string")
     */
    private $type;

}