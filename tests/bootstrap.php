<?php

if ( ! is_file($autoloadFile = __DIR__.'/../vendor/autoload.php')) {
    throw new \RuntimeException('Did not find vendor/autoload.php. Did you run "composer install --dev"?');
}

/** @var \Composer\Autoload\ClassLoader $loader */
$loader = require $autoloadFile;
$loader->addPsr4('Core12\JsonSchema\Tests\\', __DIR__);
